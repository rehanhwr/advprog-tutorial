package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.*;
import java.util.stream.Collectors;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        commands.forEach(c -> c.execute());
    }

    @Override
    public void undo() {
        commands.stream()
                .collect(Collectors.toCollection(LinkedList::new))
                .descendingIterator()
                .forEachRemaining(c -> c.undo());
    }
}
