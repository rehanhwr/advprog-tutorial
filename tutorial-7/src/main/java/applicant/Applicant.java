package applicant;

import java.util.function.Predicate;

/**
 * 4th exercise.
 */
public class Applicant {

    public boolean isCredible() {
        return true;
    }

    public int getCreditScore() {
        return 700;
    }

    public int getEmploymentYears() {
        return 10;
    }

    public boolean hasCriminalRecord() {
        return true;
    }

    public static boolean evaluate(Applicant applicant, Evaluator evaluator) {
        return evaluator.evaluate(applicant);
    }

    private static void printEvaluation(boolean result) {
        String msg = "Result of evaluating applicant: %s";
        msg = result ? String.format(msg, "accepted") : String.format(msg, "rejected");

        System.out.println(msg);
    }

    public static void main(String[] args) {
        Applicant applicant = new Applicant();
        Predicate<Applicant> creditPredicate = applicant1 -> applicant1.getCreditScore() >  600;
        Predicate<Applicant> qualifiedPredicate = applicant1 -> applicant1.isCredible();
        Predicate<Applicant> employmentPredicate =
            applicant1 -> applicant1.getEmploymentYears() > 0;
        Predicate<Applicant> criminalRecordsPredicate =
            applicant1 -> !applicant1.hasCriminalRecord();

        printEvaluation(creditPredicate.and(qualifiedPredicate).test(applicant));
        printEvaluation(creditPredicate.and(employmentPredicate.and(qualifiedPredicate))
            .test(applicant));
        printEvaluation(criminalRecordsPredicate.and(employmentPredicate.and(qualifiedPredicate))
            .test(applicant));
        printEvaluation(criminalRecordsPredicate.and(creditPredicate.and(employmentPredicate
            .and(qualifiedPredicate))).test(applicant));

    }
}
