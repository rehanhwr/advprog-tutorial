import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {

    private static Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Delta", 15);
    }

    @Test
    public void testScoreGroupingSucceed() {
        List<String> list1 = Arrays.asList("Alice");
        List<String> list2 = Arrays.asList("Bob", "Delta");
        Map<Integer, List<String>> map = new HashMap<>();
        map.put(12, list1);
        map.put(15, list2);
        assertEquals(map, ScoreGrouping.groupByScores(scores));
    }

    @Test
    public void testScoreGroupingFailed() {
        Map<Integer, List<String>> map = new HashMap<>();
        assertNotEquals(map, ScoreGrouping.groupByScores(scores));
    }
}