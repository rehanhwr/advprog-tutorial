package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {

    private static Applicant applicant;

    @Before
    public void setUp() {
        applicant = new Applicant();
    }

    @Test
    public void testEvaluateMethod() {
        assertTrue(Applicant.evaluate(applicant, new CreditEvaluator(new QualifiedEvaluator())));
        assertTrue(Applicant.evaluate(applicant,
                new CreditEvaluator(new EmploymentEvaluator(new QualifiedEvaluator()))));
        assertFalse(Applicant.evaluate(applicant,
                new CriminalRecordsEvaluator(
                        new EmploymentEvaluator(new QualifiedEvaluator()))));
        assertFalse(Applicant.evaluate(applicant,
                new CriminalRecordsEvaluator(
                        new CreditEvaluator(
                                new EmploymentEvaluator(new QualifiedEvaluator())))));
    }
}
