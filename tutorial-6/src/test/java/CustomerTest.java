import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class CustomerTest {

    private Customer customer;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);

        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        Movie movie1 = new Movie("Coco", Movie.CHILDREN);
        Movie movie2 = new Movie("Black Panther", Movie.NEW_RELEASE);
        Rental rent1 = new Rental(movie1, 4);
        Rental rent2 = new Rental(movie2, 4);

        customer.addRental(rent1);
        customer.addRental(rent2);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
        assertTrue(result.contains("<P>You owe <EM> 15.0 </EM><P>"));
        assertTrue(result.contains("On this rental you earned <EM> 3 </EM>"
                + " frequent renter points<P>"));
    }
}