import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {

    private Movie movie;
    private Movie movie2;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Coco", Movie.CHILDREN);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void testEqualsAndHashCodeMethod() {
        assertFalse(movie.equals(movie2));
        assertFalse(movie.equals(null));

        movie.setTitle("Coco");
        movie.setPriceCode(Movie.CHILDREN);

        assertTrue(movie.equals(movie2));
    }

    @Test
    public void testHashCodeMethod() {
        assertFalse(movie.hashCode() == movie2.hashCode());

        movie.setTitle("Coco");
        movie.setPriceCode(Movie.CHILDREN);

        assertTrue(movie.hashCode() == movie2.hashCode());
    }
}