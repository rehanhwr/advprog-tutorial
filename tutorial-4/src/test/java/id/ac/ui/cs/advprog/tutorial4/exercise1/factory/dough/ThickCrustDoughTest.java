package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {

    private ThickCrustDough thickCrustDough;

    @Before
    public void setUp() throws Exception{
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("ThickCrust style extra thick crust dough", thickCrustDough.toString());
    }
}
