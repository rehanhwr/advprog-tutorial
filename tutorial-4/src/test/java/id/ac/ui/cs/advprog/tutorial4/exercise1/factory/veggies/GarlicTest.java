package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GarlicTest {

    private Garlic garlic;

    @Before
    public void setUp() throws Exception{
        garlic = new Garlic();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Garlic", garlic.toString());
    }
}