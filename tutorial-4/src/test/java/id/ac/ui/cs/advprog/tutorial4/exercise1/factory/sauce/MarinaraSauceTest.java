package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {

    private MarinaraSauce marinaraSauce;

    @Before
    public void setUp() throws Exception{
        marinaraSauce = new MarinaraSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Marinara Sauce", marinaraSauce.toString());
    }
}
