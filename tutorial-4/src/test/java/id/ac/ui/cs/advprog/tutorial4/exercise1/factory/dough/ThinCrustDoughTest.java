package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinCrustDoughTest {

    private ThinCrustDough thinCrustDough;

    @Before
    public void setUp() throws Exception{
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Thin Crust Dough", thinCrustDough.toString());
    }
}
