package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SpinachTest {

    private Spinach spinach;

    @Before
    public void setUp() throws Exception{
        spinach = new Spinach();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Spinach", spinach.toString());
    }
}