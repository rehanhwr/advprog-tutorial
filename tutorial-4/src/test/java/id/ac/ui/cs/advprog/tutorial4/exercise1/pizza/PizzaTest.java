package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PizzaTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Spy
    Pizza pizza;

    @Mock
    PizzaIngredientFactory pizzaIngredientFactoryMock;

    @Mock
    Dough doughMock;

    @Mock
    Sauce sauceMock;

    @Mock
    Veggies veggiesMock;
    Veggies[] veggiesMockArray;

    @Mock
    Cheese cheeseMock;

    @Mock
    Clams clamMock;

    @Before
    public void setUp() {
        when(doughMock.toString()).thenReturn("Mock dough");
        when(sauceMock.toString()).thenReturn("Mock sauce");
        when(cheeseMock.toString()).thenReturn("Mock cheese");

        when(pizzaIngredientFactoryMock.createCheese()).thenReturn(cheeseMock);
        when(pizzaIngredientFactoryMock.createDough()).thenReturn(doughMock);
        when(pizzaIngredientFactoryMock.createSauce()).thenReturn(sauceMock);
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Test
    public void testBake() {
        pizza.bake();
        assertEquals("Bake for 25 minutes at 350\n", outContent.toString());
    }

    @Test
    public void testCut() {
        pizza.cut();
        assertEquals("Cutting the pizza into diagonal slices\n", outContent.toString());
    }

    @Test
    public void testBox() {
        pizza.box();
        assertEquals("Place pizza in official PizzaStore box\n", outContent.toString());
    }

    @Test
    public void testSetName() {
        pizza.setName("Halo");
        assertEquals("Halo", pizza.name);
    }

    @Test
    public void testGetName() {
        pizza.name = "Halo";
        assertEquals("Halo", pizza.getName());
    }

    @Test
    public void testToString() {
        pizza.toString();
    }
}
