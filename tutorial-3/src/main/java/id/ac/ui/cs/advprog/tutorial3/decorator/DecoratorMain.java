package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;

public class DecoratorMain {

    public static void main(String[] args) {
        Food food = new NoCrustSandwich();
        food = new Cheese(food);
        System.out.println(food.getDescription() + " $" + food.cost());

        Food food2 = new ThickBunBurger();
        food2 = new BeefMeat(food2);
        food2 = new TomatoSauce(food2);
        food2 = new Cucumber(food2);
        food2 = new Cheese(food2);
        food2 = new Cheese(food2); //double cheese
        System.out.println(food2.getDescription() + " $" + food2.cost());

        Food food3 = new CrustySandwich();
        food3 = new ChickenMeat(food3);
        food3 = new ChiliSauce(food3);
        food3 = new Lettuce(food3);
        System.out.println(food3.getDescription() + " $" + food3.cost());

        Food food4 = new ThinBunBurger();
        food4 = new Lettuce(food4);
        food4 = new Tomato(food4);
        System.out.println(food4.getDescription() + " $" + food4.cost());
    }
}
