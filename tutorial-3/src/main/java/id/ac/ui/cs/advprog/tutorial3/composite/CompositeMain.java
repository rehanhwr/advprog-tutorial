package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.ArrayList;
import java.util.List;

public class CompositeMain {

    public static void main(String[] args) {
        List<Employees> employeesList = new ArrayList<>();
        employeesList.add(new Ceo("da Boss", 200002));
        employeesList.add(new Cto("da tech Boss", 100001));

        employeesList.forEach(e -> System.out.println(e.getName()));

        Company stratup = new Company(employeesList);
        System.out.printf("Hi, we are Stratup, new startup.\n");
        System.out.printf("Members: \n");
        printCompanyMembers(stratup);

        stratup.addEmployee(new BackendProgrammer("backend guy", 20002.00));
        stratup.addEmployee(new FrontendProgrammer("frontend girl", 30003));
        stratup.addEmployee(new NetworkExpert("network geek", 50005));
        stratup.addEmployee(new SecurityExpert("hacker man", 70007));
        stratup.addEmployee(new UiUxDesigner("artsy woman", 90009));

        System.out.println("New Member added!");
        System.out.printf("Members: \n");
        printCompanyMembers(stratup);
        System.out.printf("Total salaries = %.2f\n", stratup.getNetSalaries());
    }

    public static void printCompanyMembers(Company company) {
        int[] idx = { 0 };
        company.getAllEmployees().forEach(e -> System.out.println(++idx[0] + ". " + e.getName()));
    }
}
